# Kenshi Interactive Map

Greetings. A small group from the Kenshi Community Discord started this project about 3 months ago. Today, we're proud to release the first publicly available version of the Kenshi Interactive Map. You can share coordinates for locations with friends for bases, locate recruits and bounties easily, there's a built-in ruin tracker to help you remember whether you have been to a ruin or not, and you can add your own custom markers to the map to mark areas of interest or your own base locations. Keep in mind the map is not a 100% finished product, and development will continue. Stop by the Kenshi Community Discord in the interactive-map-support channel with questions and/or feedback. The link for the community Discord is as follows:

https://discord.gg/ZUTCUNg

--------------
INSTALL
--------------

Download the Zip file to a location on your computer. Extract the contents of the Zip file. Open the location of the install and click on the index.html file to open. It is HIGHLY recommended that you use Chrome for viewing the map as that was the browser used for development.You can right click the index.html file and click "create shortcut" to launch the map from anywhere.

// Icons

var crosshairIcon = L.icon({
  iconUrl: 'assets/images/mapIcons/crosshairsnewai.png',
  iconSize: [180, 180], // size of the icon
  iconAnchor: [90, 90], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -90]
});

var refMarker = L.marker(myMap.getCenter(), {
  icon: crosshairIcon,
  clickable: false,
  draggable: true,
  zIndexOffset: 5000
});
refMarker.bindPopup("").openPopup();


var coordsInput;
var searchBtn;
var coordsPopUp;

function zoomToCoords(e) {
  coordsPopUp = L.popup();
  var container = document.getElementById("searchContainer").cloneNode(true);
  coordsPopUp.setLatLng(e.latlng).setContent(container).openOn(myMap);
  coordsInput = container.querySelector('#coordsInput');
  searchBtn = container.querySelector('#searchBtn');
}

// Shows coords on the reference marker on dragend

refMarker.on('dragend', function() {
  //Popup with lat and long
  var coords = refMarker.getLatLng();
  var results = "[" + coords.lat.toFixed(2) + "," + coords.lng.toFixed(2) + "]";
  refMarker.getPopup().setContent("<b>" + results + "<b>").openOn(myMap);
});

refMarker.on('click', function() {
  //Popup with lat and long
  var coords = refMarker.getLatLng();
  var results = "[" + coords.lat.toFixed(2) + "," + coords.lng.toFixed(2) + "]";
  refMarker.getPopup().setContent("<b>" + results + "<b>").openOn(myMap);
  Clipboard.copy(results);
  refMarker.getPopup().setContent("<b>" + "Copied to Clipboard" + "<b>").openOn(myMap);
  setTimeout(function() {
    refMarker.getPopup().setContent("<b>" + results + "<b>").openOn(myMap);
  },1000);
});

refMarker.on('contextmenu', function(e) {
  var coordsSearch = document.getElementById("coordsInput");
  var zoomToBtn = document.getElementById("searchBtn");
  coordsSearch.classList.remove("hidden");
  zoomToBtn.classList.remove("hidden");
  zoomToCoords(e);
  L.DomEvent.on(searchBtn, 'click', function() {
    if (coordsInput.value != null){
      setTimeout(function() {
        myMap.zoomIn(2);
        setTimeout(function() {
          myMap.zoomIn(2);
          setTimeout(function() {
            myMap.panTo(JSON.parse(coordsInput.value));
            refMarker.setLatLng(JSON.parse(coordsInput.value));
            var coords = refMarker.getLatLng();
            var results = "[" + coords.lat.toFixed(2) + "," + coords.lng.toFixed(2) + "]";
            refMarker.getPopup().setContent(results).openOn(myMap);
            setTimeout(function() {
              myMap.zoomOut(1);
              setTimeout(function() {
                myMap.zoomOut(1);
              }, 750);
            }, 750);
          }, 500);
        }, 750);
      }, 750);
    }
  });
});


// Snaps reference marker to center of viewport on click

myMap.on('click', function() {
  refMarker.setLatLng(myMap.getCenter());
});

// Makes a layergroup for the reference marker

var refLayer = L.layerGroup([refMarker]);
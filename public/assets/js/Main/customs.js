var customIcon = L.icon({
  iconUrl: 'assets/images/mapIcons/playeroutpost.png',
  iconSize: [36, 60], // size of the icon
  iconAnchor: [18, 30], // point of the icon which will correspond to marker's location
  popupAnchor: [0, 40]
});

function refreshMarkerList() {
  var markerList = document.getElementById("custom-list");
  var sortedCustoms = customMarkers.sort(function(a, b) {
    var nameA = a.name.toUpperCase();
    var nameB = b.name.toUpperCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    // names must be equal
    return 0;
  });
  while ( markerList.firstChild ) {
    markerList.removeChild(markerList.firstChild);
  }

  sortedCustoms.forEach(function(item) {
    var customList = document.createElement("li");
    var textnode = document.createTextNode(item.name);
    customList.appendChild(textnode);
    markerList.appendChild(customList);

    customList.addEventListener('click', function() {
      setTimeout(function() {
        myMap.zoomIn(2);
        setTimeout(function() {
          myMap.zoomIn(2);
          setTimeout(function() {
            myMap.panTo([item.coords.lat, item.coords.lng]);
            setTimeout(function() {
              myMap.zoomOut(1);
              setTimeout(function() {
                myMap.zoomOut(1);
              }, 750);
            }, 750);
          }, 750);
        }, 750);
      }, 750);
    });
  });
}

// Define event for clicking on Custom Markers

var renameBtn;
var deleteBtn;
var choicePopUp;

function renameDeleteChoice(e) {

  choicePopUp = L.popup();
  var container = document.getElementById("choiceContainer").cloneNode(true);
  choicePopUp.setLatLng(e.latlng).setContent(container).openOn(myMap);
  renameBtn = container.querySelector('#renameBtn'); // input
  deleteBtn = container.querySelector('#deleteBtn'); // savebtn
}

var newName;
var saveBtn;
var renamePopup;

function renameMarker(e) {

  renamePopup = L.popup();
  var container = document.getElementById("renameContainer").cloneNode(true);
  renamePopup.setLatLng(e.latlng).setContent(container).openOn(myMap);
  newName = container.querySelector('#newName'); // input
  saveBtn = container.querySelector('#saveBtn'); // savbtn

}

// Retrieve the marker Coords from localStorage

function customMarkerData() {
  return JSON.parse(window.localStorage.getItem('customData')) || [];
}

// Creates the custom markers added by the user and stores them to localStorage
var customMarkers = [];
var customLayer = new L.layerGroup();
var storedMarkers = customMarkerData();

storedMarkers.forEach(function(item) {
  addMarker(item.name, item.coords, customIcon);
});

function addMarker (name, coords, icon) {
  var marker = L.marker(coords, {
    icon
  }).addTo(customLayer).addTo(myMap);
  marker.bindTooltip(name, {
    permanent: true,
    offset: [0, 25],
    direction: "bottom"
  });
  customLayer.addTo(myMap);
  customMarkers.push({
    name,
    coords,
  });
  customMarkers.sort();
  refreshMarkerList();

  marker.on('click', function(e) {
    var rename = document.getElementById("renameBtn");
    var deleteme = document.getElementById("deleteBtn");
    rename.classList.remove("hidden");
    deleteme.classList.remove("hidden");
    renameDeleteChoice(e);

    L.DomEvent.on(renameBtn, 'click', function() {
      var saveMe = document.getElementById("saveBtn");
      var nameMe = document.getElementById("newName");
      rename.classList.add("hidden");
      deleteme.classList.add("hidden");
      saveMe.classList.remove("hidden");
      nameMe.classList.remove("hidden");
      renameMarker(e);

      L.DomEvent.on(saveBtn, 'click', function(){

        var element = customMarkers.find(function(element) {
          return element.coords.lat === marker._latlng.lat && element.coords.lng === marker._latlng.lng;
        });
        var index = customMarkers.indexOf(element);
        marker.setTooltipContent(newName.value);
        customMarkers[index].name = newName.value;
        myMap.closePopup();
        refreshMarkerList();

        window.localStorage.setItem('customData', JSON.stringify(customMarkers));
      });

    });


    L.DomEvent.on(deleteBtn, 'click', function() {
      var element = customMarkers.find(function(element) {
        return element.coords.lat === marker._latlng.lat && element.coords.lng === marker._latlng.lng;
      });

      var index = customMarkers.indexOf(element);
      customMarkers.splice(index, 1);
      myMap.closePopup();

      customLayer.eachLayer(function (layer) {

        if (element.coords.lat === layer._latlng.lat && element.coords.lng === layer._latlng.lng) {
          customLayer.removeLayer(layer);
        }
      });

      refreshMarkerList();

      window.localStorage.setItem('customData', JSON.stringify(customMarkers));
    });
  });

  // Save marker Coords to localStorage

  window.localStorage.setItem('customData', JSON.stringify(customMarkers));
}

myMap.on('contextmenu', function(e) {
  var saveMe = document.getElementById("saveBtn");
  var nameMe = document.getElementById("newName");
  saveMe.classList.remove("hidden");
  nameMe.classList.remove("hidden");
  renameMarker(e);

  L.DomEvent.on(saveBtn, 'click', function() {
    if (nameMe.value != null) {
      myMap.closePopup();
      addMarker(newName.value, e.latlng, customIcon);
    }
  });
});
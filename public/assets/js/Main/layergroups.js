var townLayer = new L.layerGroup();
var ruinLayer = new L.layerGroup();
var outpostLayer = new L.layerGroup();
var poiLayer = new L.layerGroup();
var villageLayer = new L.layerGroup();
var slaveCampLayer = new L.layerGroup();
var bountyLayer = new L.layerGroup();

var dynamicRuinMarkers = [];
function ruinMarkerData() {
  return JSON.parse(window.localStorage.getItem('ruinData'));
}
var savedRuinMarkers = ruinMarkerData();

function getSavedMarkers() {
  var arr;
  if (savedRuinMarkers && savedRuinMarkers.length > 0) {
    arr = savedRuinMarkers;
  } else {
    arr = ruinMarkers;
  }
  return arr;
}

// Array for layer groups and marker groups
var markerData = [
  {
    layer: townLayer,
    markers: townMarkers,
  },
  {
    layer: outpostLayer,
    markers: outpostMarkers,
  },
  {
    layer: villageLayer,
    markers: villageMarkers,
  },
  {
    layer: slaveCampLayer,
    markers: slaveCampMarkers,
  },
  {
    layer: poiLayer,
    markers: poiMarkers,
  },
  {
    layer: bountyLayer,
    markers: bountyMarkers,
  },
  {
    layer: ruinLayer,
    markers: getSavedMarkers(),
  },
];

markerData.forEach(function(data) {
  data.markers.forEach(function(item) {
    var ruinUrl = 'assets/images/mapIcons/updatedruin.png';
    var visitedUrl = 'assets/images/mapIcons/visitedruinv2.png';

    function determineIcon() {
      if (item.visited == true) {
        var arr;
        arr = L.icon({
          iconUrl: 'assets/images/mapIcons/visitedruinv2.png',
          iconSize: [30, 45], // Originally [38, 72]
          iconAnchor: [15, 22] // Originally [19, 36]
        });

      } else if (item.visited == false) {
        arr = ruinIcon
      } else {
        arr = L.icon({
          ...item.icon.options,
        });
      }
      return arr;
    }

    function determineOffset() {
      var offset;
      if (item.visited === true) {
        offset = [-4, -8];
      } else if (item.visited === false) {
        offset = [-4, -6];
      } else {
        offset = item.offset;
      }
      return offset;
    }

    function determineDirection() {
      var direction;
      if (item.icon === bountyIcon) {
        direction = "left";
      } else {
        direction = "top";
      }
      return direction;
    }


    var marker = L.marker(item.coords, {
      icon: determineIcon()
    });
    marker.bindTooltip("<b>" + item.name + "<b>", {
      permanent: true,
      offset: determineOffset(),
      direction: determineDirection()
    })
      .addTo(data.layer);

    // Ruin markers are added to dynamic array
    if (item.visited === false || item.visited === true) {
      dynamicRuinMarkers.push({
        name: item.name,
        icon: determineIcon(),
        coords: item.coords,
        visited: item.visited,
        type : item.type,
        zone: item.zone

      });
    }
    marker.on('click', function(e) {
      console.log(e);
      var iconUrlPath = e.target.options.icon.options.iconUrl;
      var element = dynamicRuinMarkers.find(function(element) {
        var arrLat = element.coords[0];
        var arrLng = element.coords[1];
        var mapLat = marker._latlng.lat;
        var mapLng = marker._latlng.lng;
        return arrLat === mapLat && arrLng === mapLng;
      });
      var index = dynamicRuinMarkers.indexOf(element);

      if (!element) {
        return;
      }
      var visitedIcon = new L.Icon({
        iconUrl: 'assets/images/mapIcons/visitedruinv2.png',
        iconSize: [30, 45],
        iconAnchor: [15, 22]
      });
      var ruinIcon =  new L.Icon({
        iconUrl: 'assets/images/mapIcons/updatedruin.png',
        iconSize: [30, 45],
        iconAnchor: [15, 22]
      });

      if (iconUrlPath === ruinUrl) {
        e.target.setIcon(visitedIcon);
        marker.unbindTooltip();
        marker.bindTooltip(item.name, {
          permanent: true,
          offset: [-4, -8],
          direction: "top"
        });

        dynamicRuinMarkers[index].icon = visitedIcon;
        dynamicRuinMarkers[index].visited = true;

      } else if (iconUrlPath === visitedUrl) {
        e.target.setIcon(ruinIcon);
        marker.unbindTooltip();
        marker.bindTooltip(item.name, {
          permanent: true,
          offset: [-4, -6],
          direction: "top"
        });
        dynamicRuinMarkers[index].visited = false;
        dynamicRuinMarkers[index].icon = ruinIcon;
      }
      window.localStorage.setItem('ruinData', JSON.stringify(dynamicRuinMarkers));

    });
  });
});
// Disables zoom on scroll if hovering over sidebar

var sideBarContainer = document.getElementById('sidebar');

L.DomEvent.on(sideBarContainer, 'mouseover', function(){
  myMap.scrollWheelZoom.disable();

});
L.DomEvent.on(sideBarContainer, 'mouseout', function(){
  myMap.scrollWheelZoom.enable();

});

// Adds Zones to sidebar

function addUniqueData(arr, listId) {
  var markerList = document.getElementById(listId);

  arr.forEach(function(item) {
    var customList = document.createElement("li");
    if (item.type === recruit) {
      var textnode = document.createTextNode(item.name + " | " + item.fee);
    }else {
      var textnode = document.createTextNode(item.name);
    }
    customList.appendChild(textnode);
    markerList.appendChild(customList);

    customList.addEventListener('click', function() {
      setTimeout(function() {
        myMap.zoomIn(2);
        setTimeout(function() {
          myMap.zoomIn(2);
          setTimeout(function() {
            myMap.panTo([item.coords[0], item.coords[1]]);
            refMarker.setLatLng(myMap.getCenter());
            setTimeout(function() {
              myMap.zoomOut(1);
              setTimeout(function() {
                myMap.zoomOut(1);
              }, 750);
            }, 750);
          }, 1000);
        }, 500);
      }, 100);
    });
  });
}
addUniqueData(bountyMarkers, "bounties-list");
addUniqueData(recruitMarkers, "recruits-list");
addUniqueData(townMarkers, "towns-list");
addUniqueData(zones, "zone-list");